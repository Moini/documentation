# Polish translation of Inkscape tutorials.
# This file is distributed under the same license as the inkscape package.
#
# Polish Inkscape Translation Team <inkscapeplteam@gmail.com>, 2008-2009.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Interpolate\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2009-10-10 11:44+0100\n"
"Last-Translator: Leszek(teo)Życzkowski <leszekz@gmail.com>\n"
"Language-Team: Polish <inkscapeplteam@gmail.com>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Poedit-Language: Polish\n"
"X-Poedit-Country: POLAND\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Leszek (teo) Życzkowski (Polski Zespół lokalizacyjny Inkscape) "
"<inkscapeplteam@gmail.com>, 2008, 2009\n"
"Marcin Florya (Polski Zespół lokalizacyjny Inkscape) <inkscapeplteam@gmail."
"com>, 2008, 2009\n"
"Wojciech Szczęsn (Polski Zespół lokalizacyjny Inkscape) "
"<inkscapeplteam@gmail.com>, 2008, 2009\n"
"Wojciech Ryrych (Polski Zespół lokalizacyjny Inkscape) <inkscapeplteam@gmail."
"com>, 2008, 2009\n"
"Piotr Parafiniuk (Polski Zespół lokalizacyjny Inkscape) "
"<inkscapeplteam@gmail.com>, 2008, 2009"

#. (itstool) path: articleinfo/title
#: tutorial-interpolate.xml:6
msgid "Interpolate"
msgstr "Interpolacja"

#. (itstool) path: articleinfo/subtitle
#: tutorial-interpolate.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-interpolate.xml:11
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr "Ten poradnik wyjaśnia, jak używać rozszerzenia „Interpolacja”"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:16
msgid "Introduction"
msgstr "Wstęp"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:17
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""
"Interpolacja jest <firstterm>liniowym wypełnieniem</firstterm> pomiędzy "
"dwoma lub więcej zaznaczonymi ścieżkami. Oznacza to wypełnienie odstępu "
"pomiędzy ścieżkami i przekształcanie go według liczby zadanych kroków."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:22
#, fuzzy
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate "
"Between Paths</guimenuitem></menuchoice> from the menu."
msgstr ""
"Aby użyć efektu interpolacji, zaznacz ścieżki, które chcesz przekształcić i "
"użyj polecenia „<command> Interpolacja…</command>” dostępnego z poziomu menu "
"„Rozszerzenia » Wygeneruj ze ścieżki…”."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:27
#, fuzzy
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap function=\"shift\">Shift</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""
"Przed uruchomieniem efektu, obiekt, który zamierzasz przekształcić, musisz "
"zamienić w <emphasis>ścieżkę</emphasis>. Wykonuje się poprzez zaznaczenie "
"obiektu i użycie polecenia „<command>Obiekt w ścieżkę</command>” dostępnego "
"z poziomu menu „Ścieżka”."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:36
#, fuzzy
msgid "Interpolation between two identical paths"
msgstr "Interpolacja pomiędzy dwoma różnymi ścieżkami"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:37
#, fuzzy
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"Najprostszym sposobem użycia efektu interpolacji jest interpolacja pomiędzy "
"dwoma takimi samymi ścieżkami. W wyniku zastosowania tego efektu przestrzeń "
"pomiędzy dwoma ścieżkami zostaje wypełniona kopiami oryginalnych ścieżek. "
"Liczba kroków określa, ile duplikatów zostanie umieszczonych w tej "
"przestrzeni."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:42 tutorial-interpolate.xml:75
msgid "For example, take the following two paths:"
msgstr "Dla przykładu weźmy następujące dwie ścieżki:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:52
#, fuzzy
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr ""
"Zaznacz obie ścieżki i uruchom efekt interpolacji z ustawieniami widocznymi "
"na obrazku."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:62
#, fuzzy
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""
"Jak można zauważyć powyżej, przestrzeń pomiędzy dwoma okrągłymi kształtami "
"została wypełniona sześcioma (tyle zostało określonych kroków interpolacji) "
"innymi okrągłymi kształtami. Zwróć uwagę, że kształty te zostały zgrupowane."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:69
msgid "Interpolation between two different paths"
msgstr "Interpolacja pomiędzy dwoma różnymi ścieżkami"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:70
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""
"Gdy interpolacja jest wykonywana pomiędzy dwoma różnymi ścieżkami, program "
"interpoluje kształt jednej ścieżki do drugiej. W wyniku tego otrzymuje się "
"płynne przejście z jednej ścieżki do drugiej z regularnością określoną przez "
"liczbę kroków interpolacji."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:85
#, fuzzy
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr ""
"Zaznacz dwie ścieżki i uruchom efekt interpolacji. Otrzymasz następujący "
"wynik:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:95
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"Jak można zauważyć powyżej, przestrzeń pomiędzy okrągłym kształtem a "
"trójkątnym, została wypełniona sześcioma ścieżkami z przejściem kształtu od "
"jednej do drugiej ścieżki."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:99
#, fuzzy
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""
"Gdy interpolacja jest wykonywana pomiędzy dwoma różnymi ścieżkami, bardzo "
"ważna jest <emphasis>pozycja</emphasis> początkowego węzła każdej ścieżki. "
"Aby znaleźć węzeł początkowy ścieżki, zaznacz ścieżkę i wybierz narzędzie "
"„Edycja węzłów” – węzły zostaną wyświetlone – i naciśnij klawisz "
"[ <keycap>TAB</keycap> ]. Pierwszy zaznaczony węzeł jest węzłem początkowym "
"ścieżki. "

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:105
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"Popatrz na obrazek znajdujący się poniżej. Jest on identyczny, jak w "
"poprzednim przykładzie, ale są wyświetlone węzły. Węzły wyświetlone na "
"zielono są węzłami początkowymi."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:116
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr ""
"Poprzedni przykład (teraz wyświetlany poniżej) został wykonany przy użyciu "
"tych węzłów początkowych."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:126
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr ""
"Zaobserwuj zmiany podczas lustrzanego odbijania trójkątnego kształtu, gdy "
"węzeł początkowy jest w innej pozycji: "

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:146
msgid "Interpolation Method"
msgstr "Metoda interpolacji"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:147
#, fuzzy
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either <guilabel>Split paths into segments of equal lengths</guilabel> or "
"<guilabel>Discard extra nodes of longer path</guilabel>."
msgstr ""
"Jednym z parametrów efektu „Interpolacja” jest „Metoda interpolacji”. Są "
"dwie metody interpolacji, które stosują różne sposoby przeliczania krzywych "
"nowych kształtów. Wybór jest pomiędzy „Metodą interpolacji 1” lub 2."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:153
#, fuzzy
msgid ""
"In the examples above, we used the first Interpolation Method (Split paths), "
"and the result was:"
msgstr ""
"W powyższym przykładzie została użyta metoda nr 2 i jej wynik jest "
"następujący:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:163
#, fuzzy
msgid "Now compare this to Interpolation Method 2 (Ignore nodes):"
msgstr "Teraz porównamy to z metodą nr 1:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:173
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"Ten poradnik nie obejmuje swoim zakresem opisu różnic w sposobie "
"przeliczania liczb przez te metody, tak więc wypróbuj te metody i zastosuj "
"tę, która Ci najbardziej odpowiada."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:179
msgid "Exponent"
msgstr "Wykładnik"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:180
#, fuzzy
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 1 makes the spacing between the "
"copies all even."
msgstr ""
"Parametr <firstterm>wykładnik</firstterm> odpowiada za odstępy pomiędzy "
"krokami interpolacji. Wartość 0 daje w rezultacie równy odstęp pomiędzy "
"kopiami."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:184
#, fuzzy
msgid "Here is the result of another basic example with an exponent of 1."
msgstr ""
"Tutaj znajduje się wynik innego podstawowego przykładu z wykładnikiem 0."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:194
#, fuzzy
msgid "The same example with an exponent of 0.5:"
msgstr "Ten sam przykład z wykładnikiem 1:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:204
#, fuzzy
msgid "with an exponent of 0.3:"
msgstr "z wykładnikiem 2:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:214
#, fuzzy
msgid "and with an exponent of 1.5:"
msgstr "i z wykładnikiem -1:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:224
#, fuzzy
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"Podczas działań z wykładnikami w efekcie „Interpolacja” bardzo ważna jest "
"<emphasis>kolejność</emphasis> zaznaczania obiektów. W powyższym przykładzie "
"gwiazda po lewej stronie została zaznaczona pierwsza, a sześciokąt po prawej "
"drugi."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:229
#, fuzzy
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 0.5:"
msgstr ""
"Zobacz wynik, gdy kształt po prawej został zaznaczony jako pierwszy. "
"Wykładnik w tym przykładzie był ustawiony na 1:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:241
msgid "Duplicate Endpaths"
msgstr "Powiel ścieżki końcowe"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:242
#, fuzzy
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"Ten parametr określa czy wygenerowana przez efekt grupa ścieżek "
"<emphasis>zawiera kopię</emphasis> oryginalnych ścieżek, z których "
"interpolacja została wygenerowana."

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:248
msgid "Interpolate Style"
msgstr "Modelowanie"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:249
#, fuzzy
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"Ten parametr jest jedną z najbardziej przydatnych funkcji, jakie ma efekt "
"interpolacji. Wskazuje on efektowi, aby podczas każdego kroku próbował "
"zmienić styl ścieżek – modelował je. Jeśli początek i koniec ścieżek mają "
"różne kolory, generowane ścieżki będą przyrostowo zmieniane."

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:254
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr "To jest przykład użycia funkcji „Modelowanie” na wypełnieniu ścieżki:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:264
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "Modelowanie oddziałuje także na kontur ścieżki:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:274
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr "Ścieżka w punkcie początkowym i końcowym nie musi być taka sama, ale:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:286
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr "Stosowanie interpolacji do imitowania nieregularnych gradientów"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:287
#, fuzzy
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""
"W Inkscape'ie nie można (jeszcze) tworzyć gradientów innych niż liniowe lub "
"radialne. Niemniej można je imitować używając efektu interpolacji i "
"modelowania interpolacji. Oto prosty przykład – narysuj dwie linie o różnych "
"konturach:"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:299
msgid "And interpolate between the two lines to create your gradient:"
msgstr "Użyj interpolacji pomiędzy tymi dwoma liniami, aby utworzyć gradient:"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:311
msgid "Conclusion"
msgstr "Podsumowanie"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:312
#, fuzzy
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""
"Jak przedstawiono powyżej, efekt Inkscape'a „Interpolacja” jest potężnym "
"narzędziem. Ten poradnik omawia podstawy tego efektu, ale eksperymentowanie "
"jest kluczem do zgłębiania innych możliwości interpolacji."

#. (itstool) path: Work/format
#: interpolate-f01.svg:49 interpolate-f02.svg:52 interpolate-f03.svg:49
#: interpolate-f04.svg:52 interpolate-f05.svg:49 interpolate-f07.svg:49
#: interpolate-f08.svg:49 interpolate-f09.svg:49 interpolate-f10.svg:49
#: interpolate-f11.svg:52 interpolate-f12.svg:52 interpolate-f13.svg:52
#: interpolate-f14.svg:52 interpolate-f15.svg:52 interpolate-f16.svg:49
#: interpolate-f17.svg:49 interpolate-f18.svg:49 interpolate-f19.svg:49
#: interpolate-f20.svg:52
msgid "image/svg+xml"
msgstr "obrazek/svg+xml"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:128 interpolate-f04.svg:119 interpolate-f11.svg:119
#, fuzzy, no-wrap
msgid "Exponent: 1.0"
msgstr "Wykładnik: 0.0"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:132 interpolate-f04.svg:123 interpolate-f11.svg:123
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "Kroki interpolacji: 6"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:136 interpolate-f04.svg:127 interpolate-f11.svg:127
#, no-wrap
msgid "Interpolation Method: Split paths into segments of equal lengths"
msgstr ""

#. (itstool) path: text/tspan
#: interpolate-f02.svg:140 interpolate-f04.svg:131 interpolate-f11.svg:131
#, fuzzy, no-wrap
msgid "Duplicate Endpaths: unchecked   "
msgstr "Powiel ścieżki końcowe: niezaznaczone"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:144 interpolate-f04.svg:135 interpolate-f11.svg:135
#, fuzzy, no-wrap
msgid "Interpolate Style: unchecked  "
msgstr "Modelowanie: niezaznaczone"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:148 interpolate-f04.svg:139 interpolate-f11.svg:139
#, no-wrap
msgid "Use Z-order: unchecked"
msgstr ""

#, no-wrap
#~ msgid "Interpolation Method: 2"
#~ msgstr "Metoda interpolacji: 2"

#, fuzzy
#~ msgid "ryanlerch at gmail dot com"
#~ msgstr "Ryan Lerch, ryanlerch at gmail dot com"

#~ msgid "Interpolation between two of the same path"
#~ msgstr "Interpolacja pomiędzy dwoma takimi samymi ścieżkami"
